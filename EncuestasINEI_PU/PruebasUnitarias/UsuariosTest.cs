﻿using EncuestasINEI.Controllers;
using EncuestasINEI.Models;
using Moq;
using NUnit.Framework;
using System.Web.Mvc;

namespace PruebasUnitarias
{
    [TestFixture]
    public class UsuariosTest
    {
        //Como se quiere probar los CRUDs de la clase UsuariosController, se realiza
        //el mock de dicha clase
        Moq.Mock<IUsuariosController> mockUsuarioController = new Moq.Mock<IUsuariosController>();

        //No se realiza el mock a la entidad Usuario porque es creado
        //en automatico por el entity framework. Por tanto, dicha entidad esta
        //disponible desde que es creado el proyecto   
        Usuario usuario = new Usuario();

        //Como los metodos CRUD devuleven objetos del tipo RedirectToRouteResult
        //Se declara dicho objeto para indicar al mock lo que debe devolver
        RedirectToRouteResult Redirect = new RedirectToRouteResult(
            new System.Web.Routing.RouteValueDictionary {
                { "action", "Login" }, { "controller", "Index" } });

        [Test]
        [Category("CRUD_Usuarios")]
        public void Create_Always_ReturnObjectUsuario()
        {
            //No se realiza el mock a la entidad Usuario porque es creado
            //en automatico por el entity framework  
            usuario.ID = 1;
            usuario.Nombre = "Sergio";
            usuario.Username = "sergio";
            usuario.Password = "123";
            usuario.Email = "sergio@hotmail";
            usuario.TipoUsuarioID = 1;

            //Se realiza el mocking, indicando lo que debería devolver el método Create
            mockUsuarioController.Setup(uc => uc.Create(It.IsAny<Usuario>())).Returns(Redirect);
            //Se realiza la prueba unitaria
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), mockUsuarioController.Object.Create(usuario));
        }

        [Test]
        [Category("CRUD_Usuarios")]
        public void DeleteConfirmed_Always_ReturnObjectUsuario()
        {
            //No se realiza el mock a la entidad Usuario porque es creado
            //en automatico por el entity framework            
            usuario.ID = 1;
            //Se realiza el mocking, indicando lo que debería devolver el método DeleteConfirmed
            mockUsuarioController.Setup(u => u.DeleteConfirmed(usuario.ID)).Returns(Redirect);
            //Se realiza la prueba unitaria
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), mockUsuarioController.Object.DeleteConfirmed(usuario.ID));
        }

        [Test]
        [Category("CRUD_Usuarios")]
        public void Edit_Always_ReturnObjectUsuario()
        {
            //No se realiza el mock a la entidad Usuario porque es creado
            //en automatico por el entity framework  
            usuario.ID = 1;
            usuario.Nombre = "Sergio";
            usuario.Username = "sergio";
            usuario.Password = "123";
            usuario.Email = "sergio@hotmail";
            usuario.TipoUsuarioID = 1;

            //Se realiza el mocking, indicando lo que debería devolver el método Edit
            mockUsuarioController.Setup(uc => uc.Edit(usuario)).Returns(Redirect);
            //Se realiza la prueba unitaria
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), mockUsuarioController.Object.Edit(usuario));
        }

        [Test]
        [Category("CRUD_Usuarios")]
        public void Index_Always_ReturnObjectUsuario()
        {
            //Se realiza el mocking, indicando lo que debería devolver el método Index
            mockUsuarioController.Setup(u => u.Index()).Returns(Redirect);
            //Se realiza la prueba unitaria
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), mockUsuarioController.Object.Index());
        }
    }
}
