﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using EncuestasINEI.SecurityFilters;
using System.Data.SqlClient;
using System.Text;

namespace EncuestasINEI.Controllers
{
    [LoggedOnAttribute]
    public class TipoPreguntasController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();

        // GET: TipoPreguntas
        public ActionResult Index()
        {
            return View(db.TipoPregunta.ToList());
        }

        // GET: TipoPreguntas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipoPregunta = db.TipoPregunta.Find(id);
            if (tipoPregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipoPregunta);
        }

        // GET: TipoPreguntas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoPreguntas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre")] TipoPregunta tipoPregunta)
        {
            if (ModelState.IsValid)
            {
                db.TipoPregunta.Add(tipoPregunta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoPregunta);
        }

        // GET: TipoPreguntas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipoPregunta = db.TipoPregunta.Find(id);
            if (tipoPregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipoPregunta);
        }

        // POST: TipoPreguntas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nombre")] TipoPregunta tipoPregunta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoPregunta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoPregunta);
        }

        // GET: TipoPreguntas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipoPregunta = db.TipoPregunta.Find(id);
            if (tipoPregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipoPregunta);
        }

        // POST: TipoPreguntas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoPregunta tipoPregunta = db.TipoPregunta.Find(id);
            try
            {
                db.TipoPregunta.Remove(tipoPregunta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }catch (SqlException se)
            {
                StringBuilder errorMessages = new StringBuilder();
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages.Append("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                return RedirectToAction("EliminarTipoPregunta", "Errores", new { error = errorMessages.ToString() });
            }
            catch (Exception e)
            {
                string errorMessage = "";
                errorMessage = e.Message;
                return RedirectToAction("EliminarTipoPregunta", "Errores", new { error = errorMessage });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
