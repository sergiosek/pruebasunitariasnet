﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EncuestasINEI.Models;
using System.Web.Helpers;
using System.Collections;
using EncuestasINEI.SecurityFilters;
using System.Data.SqlClient;
using System.Text;

namespace EncuestasINEI.Controllers
{
    [LoggedOnAttribute]
    public class EncuestasController : Controller
    {
        private EncuestasINEIEntitiesContext db = new EncuestasINEIEntitiesContext();

        // GET: Encuestas
        public ActionResult Index()
        {
            return View(db.Encuesta.ToList());
        }

        // GET: Encuestas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }

        // GET: Encuestas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Encuestas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre,Descripcion,Fecha,Costo,AlcanceNacional,Estado")] Encuesta encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Encuesta.Add(encuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(encuesta);
        }

        // GET: Encuestas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }

        // POST: Encuestas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nombre,Descripcion,Fecha,Costo,AlcanceNacional,Estado")] Encuesta encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(encuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(encuesta);
        }

        // GET: Encuestas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }

        // POST: Encuestas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Encuesta encuesta = db.Encuesta.Find(id);
            try
            {
                db.Encuesta.Remove(encuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }catch (SqlException se)
            {
                StringBuilder errorMessages = new StringBuilder();
                for (int i = 0; i < se.Errors.Count; i++)
                {
                    errorMessages.Append("Index #" + i + "\n" +
                        "Message: " + se.Errors[i].Message + "\n" +
                        "LineNumber: " + se.Errors[i].LineNumber + "\n" +
                        "Source: " + se.Errors[i].Source + "\n" +
                        "Procedure: " + se.Errors[i].Procedure + "\n");
                }
                return RedirectToAction("EliminarEncuesta", "Errores", new { error = errorMessages.ToString() });
            }catch (Exception e)
            {
                string errorMessage = "";
                errorMessage = e.Message;
                return RedirectToAction("EliminarEncuesta", "Errores", new { error = errorMessage });
            }



            /*Eliminar definitivamente una encuesta:
            //Encontrar los usuario_encusta
            var query1 = from u_e in db.Usuario_Encuesta.Include(u => u.Usuario).Include(e => e.Encuesta)
                         where u_e.EncuestaID == id
                         select u_e.ID;
            List <int> usuarios_encuestaID = query1.ToList();
            //Encontrar las respuestas
            var query2 = from r in db.Respuesta.Include(u_e => u_e.Usuario_Encuesta).Include(p => p.Pregunta)
                         where (usuarios_encuestaID.Contains(r.Usuario_EncuestaID))
                         select r;
            List<Respuesta> respuestas = query2.ToList();
            //Eliminar las respuestas
            foreach(Respuesta res in respuestas){
                db.Respuesta.Remove(res);
                db.SaveChanges();
            }
            //Eliminar usuario_encuesta
            var query3 = from u_e in db.Usuario_Encuesta.Include(u => u.Usuario).Include(e => e.Encuesta)
                         where u_e.EncuestaID == id
                         select u_e;
            List<Usuario_Encuesta> us_en = query3.ToList();
            foreach (Usuario_Encuesta ue in us_en)
            {
                db.Usuario_Encuesta.Remove(ue);
                db.SaveChanges();
            }

            //Encontrar las preguntas relacionadas y eliminarlas
            var query4 = from p in db.Pregunta.Include(en => en.Encuesta)
                         where p.EncuestaID == id
                         select p;
            List<Pregunta> preguntas = query4.ToList();
            foreach (Pregunta p in preguntas)
            {
                db.Pregunta.Remove(p);
                db.SaveChanges();
            }
            //Eliminar laa encuesta

            Encuesta encuesta = db.Encuesta.Find(id);
            db.Encuesta.Remove(encuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
            */
        }

        // GET: EncuestasParaUsuarios
        public ActionResult EncuestasParaUsuarios()
        {
            int id_usuario = ((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;

            //Para obtener las encuestas realizadas por un usuario
            var queryUXE = from us_en in db.Usuario_Encuesta.Include(en => en.Encuesta).Include(u => u.Usuario)
                           where us_en.UsuarioID == id_usuario
                         select us_en.EncuestaID;

            //Para obtener las encuestas aun no realizadas por un usuario
            var queryEncuestas = from e in db.Encuesta
                        where e.Estado == true && !(queryUXE.Contains(e.ID))
                       select e;

            List<Encuesta> encuestas = queryEncuestas.ToList(); 

            return View(encuestas);
        }

        // GET: EncuestasRealizadas
        public ActionResult EncuestasRealizadas()
        {
            int id_usuario = ((EncuestasINEI.Models.Usuario)Session["CurrentUser"]).ID;
            //Para obtener las encuestas realizadas por un usuario en la tabla respuesta
            var queryUXE = from us_en in db.Usuario_Encuesta.Include(en => en.Encuesta).Include(u => u.Usuario)
                           where us_en.UsuarioID == id_usuario
                           select us_en.EncuestaID;

            //Para obtener las encuestas realizadas por un usuario en la tabla encuesta
            var queryEncuestas = from e in db.Encuesta
                                 where e.Estado == true && (queryUXE.Contains(e.ID))
                                 select e;

            List<Encuesta> encuestas = queryEncuestas.ToList();
            return View(encuestas);
        }

        // GET: EncuestasActivas
        public ActionResult EncuestasActivas()
        {
            var query = from e in db.Encuesta
                        where e.Estado == true
                        select e;

            return View(query.ToList());
        }

        // GET: EncuestasPorCosto
        public ActionResult EncuestasPorCosto()
        {
            List<Encuesta> encuestas = (List<EncuestasINEI.Models.Encuesta>)Session["Encuestas"];
            return View(encuestas);
        }

        // POST: EncuestasPorCosto
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EncuestasPorCosto([Bind(Include = "Costo1, Costo2, Activo, Inactivo")] decimal costo1, decimal costo2, bool activo, bool inactivo)
        {
            List<Encuesta> enc = new List<Encuesta>();
            if (ModelState.IsValid)
            {
                if (costo1 > costo2)
                {
                    ViewBag.Resultado = "Rango inválido.";
                }
                else
                {
                    var query = new List<Encuesta>().DefaultIfEmpty();

                    if ((Convert.ToInt32(activo) - Convert.ToInt32(inactivo)) == 0)
                    {
                        query = from e in db.Encuesta
                                where e.Costo >= costo1 && e.Costo <= costo2
                                select e;
                    }
                    else if (activo)
                    {
                        query = from e in db.Encuesta
                                where (e.Estado == true) && (e.Costo >= costo1 && e.Costo <= costo2)
                                select e;
                    }
                    else
                    {
                        query = from e in db.Encuesta
                                where (e.Estado == false) && (e.Costo >= costo1 && e.Costo <= costo2)
                                select e;
                    }

                    enc = query.ToList();
                    if (enc.Count == 0)
                    {
                        ViewBag.Resultado = "No se encontraron encuestas en este rango. Por favor ingrese otro rango.";
                    }
                    else
                    {
                        Session["Encuestas"] = enc;
                        return RedirectToAction("EncuestasPorCosto");
                    }
                }           
            }

            return View(enc);
        }

        // GET: TopEncuestas
        public ActionResult TopEncuestas()
        {
            List<Encuesta> encuestas = null;
            return View(encuestas);
        }

        //Reporte:TopEncuestas
        public ActionResult Reporte5()
        {
            var query = from e in db.Encuesta
                        from ue in db.Usuario_Encuesta
                        where (ue.EncuestaID == e.ID)
                        group e by e.Nombre into grp
                        orderby grp.Count() ascending
                        select new { key = grp.Key, cnt = grp.Count() };

            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            query.ToList().ForEach(rs => xValue.Add(rs.key));
            query.ToList().ForEach(rs => yValue.Add(rs.cnt));

            string myTheme = @"<Chart BackColor=""Transparent"">
                                    <ChartAreas>
                                        <ChartArea Name=""Default"" BackColor=""Transparent"">
                                        </ChartArea>
                                    </ChartAreas>
                                </Chart>";

            new Chart(width: 600, height: 400, theme: myTheme).
                AddTitle("Encuestas más respondidas").
                AddSeries(chartType: "Bar", xValue: xValue, yValues: yValue).
                Write("png");
            return null;
        }

        //POST Reporte 3: Encuestas activas en un rango de fechas
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EncuestasPorFecha([Bind(Include = "Fecha1, Fecha2, Activo, Inactivo")] DateTime fecha1, DateTime fecha2, bool activo, bool inactivo)
        {
            List<Encuesta> encuestas = new List<Encuesta>();
            if (ModelState.IsValid)
            {
                if (fecha1 > fecha2)
                {
                    ViewBag.Resultado = "Rango de fecha inválido.";
                }
                else {
                    var query = new List<Encuesta>().DefaultIfEmpty();
                    if ((Convert.ToInt32(activo) - Convert.ToInt32(inactivo) == 0))
                    {
                        query = from e in db.Encuesta
                                where (e.Fecha >= fecha1 && e.Fecha <= fecha2)
                                orderby e.Fecha ascending
                                select e;
                    }
                    else if (activo)
                    {
                        query = from e in db.Encuesta
                                where (e.Estado == true) && (e.Fecha >= fecha1 && e.Fecha <= fecha2)
                                orderby e.Fecha ascending
                                select e;
                    }
                    else
                    {
                        query = from e in db.Encuesta
                                where (e.Estado == false) && (e.Fecha >= fecha1 && e.Fecha <= fecha2)
                                orderby e.Fecha ascending
                                select e;
                    }

                    encuestas = query.ToList();
                    if (encuestas.Count() == 0)
                    {
                        ViewBag.Resultado = "No se encontraron encuestas activas entre estas fechas";
                    }
                    else
                    {
                        Session["Encuestas"] = encuestas;
                        return RedirectToAction("EncuestasPorFecha");
                    }
                }
            }
            return View(encuestas);
        }

        //GET Reporte 3: Encuestas activas en un rango de fechas
        public ActionResult EncuestasPorFecha()
        {
            List<Encuesta> encuestas = (List<EncuestasINEI.Models.Encuesta>)Session["Encuestas"];
            return View(encuestas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
