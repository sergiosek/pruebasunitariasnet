﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EncuestasINEI.Controllers
{
    public class ErroresController : Controller
    {
        //GET: EliminarTipoUsuario
        public ActionResult EliminarTipoUsuario(string error)
        {
            ViewBag.Error = error;
            return View();
        }

        //GET: EliminarTipoPregunta
        public ActionResult EliminarTipoPregunta(string error)
        {
            ViewBag.Error = error;
            return View();
        }

        //GET: EliminarPregunta
        public ActionResult EliminarPregunta(string error)
        {
            ViewBag.Error = error;
            return View();
        }

        //GET: EliminarEncuesta
        public ActionResult EliminarEncuesta(string error)
        {
            ViewBag.Error = error;
            return View();
        }

        // GET: Errores
        public ActionResult Index()
        {
            return View();
        }

        // GET: Errores/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Errores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Errores/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Errores/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Errores/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Errores/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Errores/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
