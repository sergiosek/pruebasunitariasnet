﻿using System.Web.Mvc;
using EncuestasINEI.Models;

namespace EncuestasINEI.Controllers
{
    public interface IUsuariosController
    {
        ActionResult Create([Bind(Include = "ID,Nombre,Username,Password,Email,TipoUsuarioID")] Usuario usuario);
        ActionResult DeleteConfirmed(int id);
        ActionResult Edit([Bind(Include = "ID,Nombre,Username,Password,Email,TipoUsuarioID")] Usuario usuario);
        ActionResult Index();
    }
}