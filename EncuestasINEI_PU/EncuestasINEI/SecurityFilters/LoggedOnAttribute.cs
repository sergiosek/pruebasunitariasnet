﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EncuestasINEI.SecurityFilters
{
    public class LoggedOnAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Session["CurrentUser"] != null;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["ErrorMessage"] = "¡Necesita loguearse para acceder a esta opción!";
            filterContext.Result = new RedirectToRouteResult(
                                   new System.Web.Routing.RouteValueDictionary
                                   {
                                       {"action", "Login" },
                                       {"Controller", "Usuarios" }
                                   });
        }
    }
}