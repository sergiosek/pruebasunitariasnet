﻿using EncuestasINEI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EncuestasINEI.SecurityFilters
{
    public class IsEncuestadoAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool result = base.AuthorizeCore(httpContext);
            Usuario objUsuario = (Usuario)httpContext.Session["CurrentUser"];
            if (objUsuario.TipoUsuario.Nombre.ToLower() == "encuestado")
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["ErrorMessage"] = "¡Ud. no puede acceder a esta opción!";
            filterContext.Result = new RedirectToRouteResult(
                                   new System.Web.Routing.RouteValueDictionary
                                   {
                                       {"action", "Index" },
                                       {"controller", "Home" }
                                   });
        }
    }
}